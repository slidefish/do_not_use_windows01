

package ms_office_converter;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
# @EXPORT_OK   = qw(func1 func2);
# %EXPORT_TAGS = ( DEFAULT => [qw(&func1)],
                 # Both    => [qw(&func1 &func2)]);

use strict; use warnings;
use Try::Tiny;
use Getopt::Long;
use Term::ReadKey;
use File::Basename;
use File::Spec;
use IPC::Cmd qw[can_run run run_forked];

require './perl_DB/db_log_DBI.pl';
my $log_f;
$log_f = get_logger("SlideLog.info");
$log_f->debug('ppt_text_extractor Logging initialised, yay!');
$log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL
#$log_f->level($Log::Log4perl::INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL
if($Log::Log4perl::INFO || $Log::Log4perl::DEBUG) {} #squash warnings

my($debug, $files_in_dir, $i);
my($volume, $input_dir, $processing_dir, $error_dir);
my($key);

$debug = 1;
# $debug = 0;
$files_in_dir = 0;
$i = -1;
$volume = "C:";
$input_dir = "/slidefish/input";
$processing_dir = "/slidefish/processing";
$error_dir = "/slidefish/error";

$log_f->debug( sprintf("Main: input_dir: %s", $input_dir) );

while (1 && (not defined ($key = ReadKey(-1))))
{
	$i++;
	
	$files_in_dir = files_in_directory($input_dir);
	if ($files_in_dir)
	{
		while ($files_in_dir)
		{

			process_input_file($input_dir, $processing_dir, $error_dir);
			$files_in_dir = files_in_directory($input_dir);
			$i = 0;

		}
	}
	else
	{

		
		(!($i%10)) && ( printf("no file loop: %s\n", $i) );
		$| = 1;
		sleep(1);

	}
}

printf("\npress enter key to exit...\n");
ReadLine(0);

1;


sub process_input_file
{

	my($in_path, $proc_path, $err_path) = @_;
	$log_f->debug( sprintf("in_path: %sproc_path: %serr_path: %s", $in_path, $proc_path, $err_path) );
	
	my($files, $fn, $dir, $nfn, $ext);
	my($dh, $fc);
	opendir($dh, $in_path) || die "can't opendir $in_path: $!";
	$fc = scalar grep { /^[^\.]/ && -f "$in_path/$_" } readdir($dh);
	rewinddir $dh;

	$fn = $nfn = $ext = '';

	my($is_ppt, $is_xl, $is_word);
	$is_ppt = $is_xl = $is_word = 0;

	$log_f->debug( sprintf("process_input_file: Files:") );

	while(my $ofile = readdir($dh))
	{

		($ofile =~ /^.$/) && next;
		($ofile =~ /^..$/) && next;
		
		$ofile = File::Spec->catpath($volume, $in_path, $ofile);
		(-f $ofile) || next;
		
		$log_f->debug( sprintf("%s: \$ofile = %s", (caller(0))[3], $ofile) );
		#my_sub_name((caller(0))[3]);
		
		$is_ppt = $is_xl = $is_word = 0;
		
		$log_f->debug( sprintf("file: %s", $ofile) );
		#$fn = basename($ofile);
		($fn, $dir, $ext) = fileparse($ofile, '\.[^\.]*');
		$log_f->debug( sprintf("process_input_file: \$fn = %s, \$dir = %s, \$ext = %s", $fn, $dir, $ext));

		$nfn = File::Spec->catpath($volume, $proc_path, $fn . $ext);
		
		# excel
		# check if it is as valid excel (xls, xlsx, xlsm, xltx, xltm, xlsb)
		if ($ext =~ /.xls[xmbt]??$/) { $is_xl = 1 }
		elsif ($ext =~ /.xlt[xm]??$/) { $is_xl = 1 }
		# powerpoint
		# checked that it is as valid ppt, pptx, pptm, potx, potm, ppsx, ppsm, sldx, sldm
		# move the file to the processing directory
		elsif ($ext =~ /^\.p[op]t[xm]??$/) { $is_ppt = 1; } # powerpoint and template
		elsif ($ext =~ /^\.pps[xm]??$/) { $is_ppt = 1; } # powerpoint show
		elsif ($ext =~ /^\.sld[xm]??$/) { $is_ppt = 1; } # powerpoint slide
		# word
		# checked that it is as valid doc, docx, docm, dotx, dotm, dot
		# move the file to the processing directory
		elsif ($ext =~ /^\.do[tc][xm]??$/) { $is_word = 1; } # word document and template
		# error
		else { $nfn = File::Spec->catpath($volume, $err_path, $fn . $ext) }

		$log_f->debug( sprintf("\$nfn: %s", $nfn) );
		if (-f $nfn)
		{
			unlink $nfn; #try/catch exception handling needs to be implemented
		}
		rename($ofile, $nfn);

		my $outdir = 'C:/slidefish/output';

		if ($is_ppt)
		{
			$log_f->debug( sprintf("process_input_file: %s powerpoint processing started", $ofile));
			run_ppt_converter($nfn, $outdir);
		}
		elsif ($is_xl)
		{
			$log_f->debug( sprintf("process_input_file: %s Excel processing started", $ofile));
			run_xl_converter($nfn, $outdir);
		}
		elsif ($is_word)
		{
			$log_f->debug( sprintf("process_input_file: %s Word processing started", $ofile));
			run_doc_converter($nfn, $outdir);
		}
		$log_f->info( sprintf("process_input_file: %s processing completed", $ofile));
	}
	closedir $dh;
}


#############################################
#############################################

sub run_doc_converter
{
	my($in_file, $outdir) = @_;
	my $perl_script = 'word_doc_converter.pl';
	my $proc_name = File::Spec->catpath($volume, '/slidefish', $perl_script);
	
	$log_f->debug( sprintf("%s: proc_name = %s", 'run_doc_converter', $proc_name) );
	
	my ($fn, $indir, $ext) = fileparse($in_file, '\.[^\.]*');

	$log_f->debug( sprintf("run_doc_converter: \$in_file = %s, \$indir = %s, \$fn = %s, \$ext = %s", 
					$in_file, $indir, $fn, $ext) );
	
	my $cmd = ['perl', $proc_name, # '--test', '--debug',
			'--indir', $indir, 
			'--outdir', $outdir, 
			'--infilen', $fn . $ext,  
			'--pdf', $fn . $ext . '.pdf'];

	$log_f->debug( sprintf("%s: \$cmd = %s", 'run_doc_converter', join(' ', @$cmd)) );
	
	my( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf ) =
            run( command => $cmd, verbose => 1 );
	my $msg = substr(join("", @$full_buf), 0, 255);

    if( $success ) {
		#printf("%s: %s: length: %d, msg: %s\n", 'run_xl_converter', 'SUCCESS', length($msg), $msg);
        $log_f->info( sprintf("%s: Success: %s", 'run_xl_converter', $msg) );
    }
	else {
		#printf("%s: %s: length: %d, msg: %s\n", 'run_doc_converter', 'FAIL', length($msg), $msg);
        $log_f->info( sprintf("%s: FAIL: %s", 'run_doc_converter', $msg) );
	}
}

#############################################
#############################################
sub run_xl_converter
{
	my($in_file, $outdir) = @_;
	my $perl_script = 'xl_converter.pl';
	my $proc_name = File::Spec->catpath($volume, '/slidefish', $perl_script);
	
	$log_f->debug( sprintf("%s: proc_name = %s", 'run_xl_converter', $proc_name) );
	
	my ($fn, $indir, $ext) = fileparse($in_file, '\.[^\.]*');

	$log_f->debug( sprintf("run_xl_converter: \$in_file = %s, \$indir = %s, \$fn = %s, \$ext = %s", 
					$in_file, $indir, $fn, $ext) );
	
	my $cmd = ['perl', $proc_name, # '--test', '--debug',
			'--indir', $indir, 
			'--outdir', $outdir, 
			'--infilen', $fn . $ext,  
			'--pdf', $fn . $ext . '.pdf'];

	$log_f->debug( sprintf("%s: \$cmd = %s", 'run_xl_converter', join(' ', @$cmd)) );
	
	my( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf ) =
            run( command => $cmd, verbose => 1 );
	my $msg = substr(join("", @$full_buf), 0, 255);

    if( $success ) {
		#printf("%s: %s: length: %d, msg: %s\n", 'run_xl_converter', 'SUCCESS', length($msg), $msg);
        $log_f->info( sprintf("%s: Success: %s", 'run_xl_converter', $msg) );
    }
	else {
		#printf("%s: %s: length: %d, msg: %s\n", 'run_xl_converter', 'FAIL', length($msg), $msg);
        $log_f->info( sprintf("%s: FAIL: %s", 'run_xl_converter', $msg) );
	}
}
#############################################
#############################################

sub run_ppt_converter
{
	my($in_file, $outdir) = @_;
	my $proc_name = File::Spec->catpath($volume, '/slidefish', 'ppt_text_extractor.pl');
	
	$log_f->debug( sprintf("run_ppt_converter: proc_name = %s", $proc_name) );
	
	my ($fn, $indir, $ext) = fileparse($in_file, '\..*');

	$log_f->debug( sprintf("run_ppt_converter: \$in_file = %s, \$indir = %s, \$fn = %s, \$ext = %s", 
					$in_file, $indir, $fn, $ext) );
	
	my $cmd = ['perl', 'C:/slidefish/ppt_text_extractor.pl', # '--test', '--debug',
			'--indir', $indir, 
			'--outdir', $outdir, 
			'--infilen', $fn . $ext,  
			'--textf', $fn . $ext . '.txt',
			'--pdf', $fn . $ext . '.pdf'];

	$log_f->debug( sprintf("%s: \$cmd = %s", 'run_ppt_converter', join(' ', @$cmd)) );
	
	my( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf ) =
            run( command => $cmd, verbose => 0 );
	my $msg = substr(join("", @$full_buf), 0, 255);
    if( $success ) {
		
        $log_f->info( sprintf("run_ppt_converter: Success: %s", $msg) );
    }
	else {
        $log_f->info( sprintf("run_ppt_converter: FAIL: %s", $msg) );
	}
}
#############################################
#############################################

sub my_sub_name
{
	my $full_name = $_[0];
	printf("my_sub_name: \$full_name = %s", $full_name);
	
	$full_name =~ m/^.*([^:]*)$/;
	my $f = $1;
	printf("my_sub_name: returns = %s", $f);
	return $f;
}

sub files_in_directory
{

	my ($some_dir);
	$some_dir = $_[0];
	
	my($dh, $fc);
		
	opendir($dh, $some_dir) || die "can't opendir $some_dir: $!";
	$fc = scalar grep { /^[^\.]/ && -f "$some_dir/$_" } readdir($dh);
	closedir $dh;
	
	#$log_f->debug( sprintf("files_in_directory: found %d files in %s", $fc, $some_dir) );
	
	return $fc;

}


1;

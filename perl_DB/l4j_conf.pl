######### System initialization section ### 

$ENV{DBI_DRIVER} = 'mysql';

use Log::Log4perl qw(get_logger :levels); 
my $conf_db = q(

	log4perl.logger.Groceries.Food=DEBUG, DB 
	
	log4perl.appender.DB = Log::Log4perl::Appender::DBI 
	log4perl.appender.DB.datasource = dbi:mysql:database=700144_slidefish;host=166.78.8.197
	log4perl.appender.DB.username = windows01 
	log4perl.appender.DB.password = z0vyrDhgZtwGz6ClT3Do 
	log4perl.appender.DB.layout = Log::Log4perl::Layout::PatternLayout 
	log4perl.appender.DB.sql = \ 
		INSERT INTO windows01_log \ 
		(log_timestamp, level, method, message) \ 
		VALUES ('%d','%p','%M','%m') 

);

printf "before init\n";
Log::Log4perl::init(\$conf_db);

#Log::Log4perl->init("log4perl.conf"); 
my $food_logger = get_logger("Groceries");
$food_logger->info("found Groceries");
 
my $food_logger = get_logger("Groceries.Food");
$food_logger->info("found Groceries.Food");

######### Run it ########################## 

my $food = Groceries::Food->new(); 
my $food = Groceries::Food->new("Sushi"); 
$food->consume();
 
######### Application section ############# 
package Groceries::Food; 
use Log::Log4perl qw(get_logger); 

sub new { 
	my($class, $what) = @_; 
	my $logger = get_logger("Groceries::Food"); 
	if(defined $what) { 
		$logger->debug("New food: $what"); 
		return bless { what => $what }, $class; 
	}  

	$logger->fatal("No food defined"); 
	return undef; 
} 

sub consume { 
	my($self) = @_; 
	my $logger = get_logger("Groceries::Food"); 
	$logger->info("Eating $self->{what}"); 
} 

#!/usr/bin/perl

   use IPC::Cmd qw[can_run run run_forked];
 
	my $cmd = ['perl', 'C:\slidefish\ppt_text_extractor.pl',
				'--test', '--debug',
				'--indir', 'C:\slidefish\processing', 
				'--outdir', 'C:\slidefish\output', 
				'--infilen', 'Presentation1.pptx',  
				'--textf', 'Presentation1.ppt.txt',
				'--pdf', 'Presentation1.ppt.pdf'];

	my( $success, $error_message, $full_buf, $stdout_buf, $stderr_buf ) =
            run( command => $cmd, verbose => 0 );
    if( $success ) {
        print "this is what the command printed:\n";
        print join "", @$full_buf;
    }
	else {
		 print "FAIL: this is what the command printed:\n";
        print join "", @$full_buf;
	}
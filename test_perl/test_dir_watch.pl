

package ms_office_converter;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
use Win32::ChangeNotify;

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
# @EXPORT_OK   = qw(func1 func2);
# %EXPORT_TAGS = ( DEFAULT => [qw(&func1)],
                 # Both    => [qw(&func1 &func2)]);

use strict; use warnings;

my($debug, $files_in_dir, $i);
my($input_dir, $processing_dir, $error_dir);
my($key);
my(%files_to_process, $i_file_count);

$debug = 1;
# $debug = 0;
$files_in_dir = 0;
$i = 0;
$i_file_count = 0;

my $fh = open(
$input_dir = "C:/slidefish/input";
$processing_dir = "C:/slidefish/processing";
$error_dir = "C:/slidefish/error";

if ($debug) { printf("Main: input_dir: %s\n", $input_dir) };

my %last; @last{ glob $input_dir . '/*' } = (); # put them into a hash with keys but no values;

# clean out the current files in the processing directory
foreach my $k (keys %last) {
	printf("last key = $k\n");
	(-f $k) && ($files_to_process{$k} = $i_file_count++);
}
printf("scalar keys last = %d\n", scalar keys %last);
my @f = glob $input_dir . '/*';
printf("compare \@f < scalar keys last = %d\n", @f < scalar keys %last);
printf("compare \@f == scalar keys last = %d\n", @f == scalar keys %last);
printf("compare \@f > scalar keys last = %d\n", @f > scalar keys %last);
$| = 1;

$i = 0;

my $notify = Win32::ChangeNotify->new( $input_dir, 0, 'FILE_NAME' );

while( 1 ) {
	$| = 1;
	$i++;
	
	
    printf("%d: Nothing changed\n", $i), next
        unless $notify->wait(10000); # notify every 10 seconds
    $notify->reset;

    print $/, "Something changed\n";
    my @files = glob $input_dir . '/*';
    if( @files < scalar keys %last ) {
        delete @last{ @files };
        print 'These files where deleted: ';
        print for keys %last;
		print "\n";
		sleep(10);
    }
    elsif( @files > scalar keys %last ) {
        my %temp;
        @temp{ @files } = ();
        delete @temp{ keys %last };
        print 'These files where created: ';
        print for keys %temp;
		print "\n";
		sleep(10);
    }
    else {
        print "A non-deletion or creation change occured";
    }
    undef %last;
    @last{ @files } = ();
}

1;

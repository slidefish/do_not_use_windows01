#
# C:\slidefish\xls_converter.pl --test --debug
# --volume "C:"
# --indir C:\slidefish\input
# --outdir C:\slidefish\output
# --infilen Title1.xls
# --pdf title1.pdf 

use strict; use warnings;
use Try::Tiny;
use Getopt::Long;
use File::Spec;

require './perl_DB/db_log_DBI.pl';

my $log_f;
$log_f = get_logger("SlideLog.info");
$log_f->level($Log::Log4perl::DEBUG); # one of DEBUG, INFO, WARN, ERROR, FATAL
$log_f->level($Log::Log4perl::INFO); # one of DEBUG, INFO, WARN, ERROR, FATAL
if($Log::Log4perl::INFO || $Log::Log4perl::DEBUG) {} #squash warnings

use Win32::OLE qw(in with);
use Win32::OLE::Const qw( Microsoft.Excel );
use Win32::OLE::Const "Microsoft Office 15.0 Object Library";  # mso constants

use Win32::OLE::Enum;
$Win32::OLE::Warn = 3;

my $xlc = Win32::OLE::Const->Load(qw( Microsoft.Excel ));
# printf("xlc: %s\n", $xlc);
# foreach my $k (keys %$xlc) {
	# if($k =~ /.*True.*/i) { printf("Excel constants key: %s\n", $k) }
	# if($k =~ /.*False.*/i) { printf("Excel constants key: %s\n", $k) }
	# if($k =~ /.*quality.*/i) { printf("Excel constants key: %s\n", $k) }
# }

my ($xls_input_dir, $xls_output_dir, $xls_output_file, $test, $debug);
my ($input_fn, $out_pdf_fn);
my ($input_file, $pdf_output_file);
my ($volume);

$xls_input_dir = $xls_output_dir = $input_fn = '';
$xls_output_file = '';
$out_pdf_fn = '';
$input_file = $pdf_output_file = '';
$test = $debug = 0;
$volume = 'C:';

if($test) {

	$log_f->debug('xls_converter: using test variables');
	$xls_input_dir = qw( /slidefish/processing );  
	$xls_output_dir = qw( /slidefish/output ); 
	$input_fn = "perl2000.xls";
	$out_pdf_fn = "perl2000.xls.pdf";
	
 }
 
  GetOptions (
			'test!' => \$test,
			'debug!' => \$debug,
			'volume=s' => \$volume,
			'indir=s' => \$xls_input_dir,
			'outdir=s' => \$xls_output_dir,
			'infilen=s' => \$input_fn,
			'pdf=s' => \$out_pdf_fn);
			
$xls_input_dir =~ tr#\\#/#;
$xls_output_dir =~ tr#\\#/#;
$input_file = File::Spec->catpath('', $xls_input_dir, $input_fn);
$pdf_output_file = File::Spec->catpath('', $xls_output_dir, $out_pdf_fn);
$xls_output_file = File::Spec->catpath('', $xls_output_dir, $input_fn);

$log_f->debug( "xls_converter: inputdir: (" . $xls_input_dir . "), ouputdir: (" . $xls_output_dir  . ")");
$log_f->debug(sprintf("%s: %s:(%s) length: %d", 'xls_converter', 'xls Input file', $input_file, length($input_file)) );
$log_f->debug('xls_converter: xls Output file: (' . $xls_output_file  . ')');
$log_f->debug('xls_converter: pdf Output file: (' . $pdf_output_file  . ')');

(-e $xls_input_dir) || 
	$log_f->logdie("INPUT DIRECTORY FAILURE: $xls_input_dir!");
(-e $xls_output_dir) || 
	$log_f->logdie("OUTPUT DIRECTORY FAILURE: $xls_output_dir!");

my($xls, $workbook, $i_slide);
$xls = get_xls();
$xls->{DisplayAlerts}=0; 
#$xls->{Visible} = 1;
$test && ($xls->{Visible} = 1);

 # open the presentation
(-e $input_file) ||
	$log_f->logcroak(sprintf("xls_converter: input file (%s) does not exist", $input_file));
$workbook = $xls->Workbooks->Open($input_file);

$workbook || $log_f->logdie( 'xls_converter: NOCATCH: workbook loading Error');

$log_f->debug( 'xls_converter: saving pdf file: (' . $pdf_output_file . ')');

$pdf_output_file =~ tr#/#\\#;
$workbook->ExportAsFixedFormat(
    {
        Type                 => xlTypePDF,
        Filename             => $pdf_output_file,
        Quality              => xlQualityStandard,
        IncludeDocProperties => 'True',
        IgnorePrintAreas     => 'False',
        OpenAfterPublish     => 'False',
    }
);
	
$workbook->Close;
$pdf_output_file =~ tr#\\#/#;
$log_f->debug( 'xls_converter: (' . $pdf_output_file . ') export completed');


# move the input file to the output directory
rename($input_file, $xls_output_file) || die "$0: unable to move " . $input_file . " to " . $xls_output_file;
$log_f->info( 'xls_converter: (' . $xls_output_file . ') processing completed');

if ($debug) {
	use File::Copy;
	$log_f->debug( 'xls_converter: copy source (' . $xls_output_file . ') to ' . $input_file);
	copy ($xls_output_file, $input_file);
}	

#$xls->Exit;

sub get_xls {
    my $xls;

    try {
        $xls = Win32::OLE->GetActiveObject('Excel.Application');
		if($xls) {
			while(($xls) && (defined (my $xls->ActiveWorkbook))) { #it is in use
				sleep(1);
			}
		}
    }
    catch {
        die $_;
    };

    unless ( $xls ) {
        $xls = Win32::OLE->new('Excel.Application',  sub { $_[0]->Quit }
        ) or $log_f->logcroak(sprintf('Cannot start Excel: %s', Win32::OLE->LastError));
    }

	$log_f->debug( "xls_converter: get_xls: xls = (" . $xls . ")");

    return $xls;
}